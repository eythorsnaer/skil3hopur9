#ifndef ADDRELATIONDIALOG_H
#define ADDRELATIONDIALOG_H

#include <QDialog>
#include "scienceservice.h"

namespace Ui {
class AddRelationDialog;
}

class AddRelationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddRelationDialog(QWidget *parent = 0);
    ~AddRelationDialog();

    void getAllScientists();
    void displayScientists();
    void getAllComputers();
    void displayComputers();
    list<Scientist> currentScientists;
    list<Computer> currentComputers;
    ScienceService scienceService;

private slots:
    void on_add_Relation_clicked();

    void on_table_S_clicked(const QModelIndex &index);

    void on_table_C_clicked(const QModelIndex &index);

private:
    Ui::AddRelationDialog *ui;
    int scientistIndex;
    int computerIndex;
};

#endif // ADDRELATIONDIALOG_H
