#include "scienceservice.h"

ScienceService::ScienceService() {
    scientistRepository = ScientistRepository();
     computerRepository = ComputerRepository();
     relationRepository = RelationRepository();
}

ScienceService::~ScienceService() {
}

void ScienceService::addScientist(Scientist scientist) {
    scientistRepository.add(scientist);
}

void ScienceService::addComputer(Computer computer) {
    computerRepository.add(computer);
}

void ScienceService::addRelation(Relation relation) {
    relationRepository.add(relation);
}

void ScienceService::addRelationIndexes(int sci, int com) {
    relationRepository.add(sci, com);
}

list<Scientist> ScienceService::getAllScientists(){
    return scientistRepository.list();
}
list<Computer> ScienceService::getAllComputers(){
    return computerRepository.list();
}

list<Relation> ScienceService::getAllRelation() {
    return relationRepository.listAllRelations();
}


list<Scientist> ScienceService::getScientistsOrderedBy(string col, string mod) {
    return scientistRepository.list(col,mod);
}

list<Computer> ScienceService::getComputersOrderedBy(string col, string mod) {
    return computerRepository.listComp(col,mod);
}
list<Relation> ScienceService::getRelationsOrderedBy(string col, string mod) {
    return relationRepository.list(col,mod);
}
list<Scientist> ScienceService::search(string searchTerm) {
    return scientistRepository.search(searchTerm);
}

list<Computer> ScienceService::searchCom(string searchTerm) {
    return computerRepository.searchCom(searchTerm);
}

list<Relation> ScienceService::searchRelation(string searchTerm) {
    return relationRepository.search(searchTerm);
}

list<Relation> ScienceService::searchRelationCom(string searchTerm) {
    return relationRepository.searchcom(searchTerm);
}

list<ComputerTypes> ScienceService::getComputerTypes()
{
    return computerRepository.computerTypes();
}

void ScienceService::removeScientist(int id) {
    scientistRepository.remove(id);
}

void ScienceService::removeComputer(int id) {
    computerRepository.remove(id);
}

bool ScienceService::removeRelation(int sid, int cid) {
    return relationRepository.removeRelation(sid, cid);
}

