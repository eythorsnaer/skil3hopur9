#include "computer.h"

Computer::Computer()
{

    name = "";
    year = 0;
    type = "";
    built = "";
    id = 0;
    imagePath = "";

}

std::string Computer::getImagePath()
{
    return imagePath;
}
std::string Computer::getName()
{
    return name;
}
int Computer::getYear()
{
    return year;
}
std::string Computer::getType()
{
    return type;
}
int Computer::getID()
{
    return id;
}
bool Computer::getBuilt()
{
    if (built == 0)
        return false;
    else
        return true;
}
std::string Computer::getBuiltString()
{
    return BuiltString;
}

void Computer::setImagePath(std::string _imagePath)
{
    imagePath = _imagePath;
}
void Computer::setName(std::string _name)
{
    name = _name;
}
void Computer::setYear(int _year)
{
    year = _year;
}
void Computer::setType(std::string _type)
{
    type = _type;
}
void Computer::setID(int _id)
{
    id = _id;
}
void Computer::setBuilt(int i)
{
    if(i==1)
        built = true;
    else
        built = false;
}

void Computer::setBuiltString(std::string _BuiltString)
{
    BuiltString = _BuiltString;
}
