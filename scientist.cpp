#include "scientist.h"

Scientist::Scientist() {
    name = "";
    dateOfBirth = "";
    dateOfDeath = "";
    gender = "";
    id = 0;
    imagePath = "";
}

string Scientist::getImagePath()
{
    return imagePath;
}
string Scientist::getName()
{
    return name;
}
string Scientist::getYearOfBirth()
{
    return dateOfBirth;
}
string Scientist::getYearOfDeath()
{
    return dateOfDeath;
}
string Scientist::getGender()
{
    return gender;
}

int Scientist::getID()
{
    return id;
}
int Scientist::getPersonID()
{
    return personId;
}

void Scientist::setImagePath(string _imagePath)
{
    imagePath = _imagePath;
}

void Scientist::setName(string _name)
{
    name = _name;
}

void Scientist::setYearOfBirth(string _dateOfBirth)
{
    dateOfBirth = _dateOfBirth;
}

void Scientist::setYearOfDeath(string _dateOfDeath)
{
    dateOfDeath = _dateOfDeath;
}

void Scientist::setGender(string _gender)
{
    gender = _gender;
}

void Scientist::setID(int _id)
{
    id = _id;
}

void Scientist::setPersonID(int _personId)
{
    personId = _personId;
}
