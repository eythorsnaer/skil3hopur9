#include "relation.h"

Relation::Relation()
{
    sname = "";
    name = "";
    year = 0;
    type = "";
    built = "";
    builtString = "";
    computerId = 0;
    personId = 0;

}

std::string Relation::getName()
{
    return name;
}

std::string Relation::getSName()
{
    return sname;
}
std::string Relation::getType()
{
    return type;
}
std::string Relation::getBuiltString()
{
    return builtString;
}
int Relation::getYear()
{
    return year;
}
int Relation::getPersonID()
{
    return personId;
}
int Relation::getComputerID()
{
    return computerId;
}

bool Relation::getBuilt()
{
    if (built == 0)
        return false;
    else
        return true;
}
void Relation::setName(std::string _name)
{
    name = _name;
}
void Relation::setSName(std::string _sname)
{
    sname = _sname;
}
void Relation::setType(std::string _type)
{
    type = _type;
}
void Relation::setBuiltString(std::string _builtString)
{
    builtString = _builtString;
}
void Relation::setYear(int _year)
{
    year = _year;
}
void Relation::setPersonID(int _personId)
{
    personId = _personId;
}
void Relation::setComputerID(int _computerId)
{
    computerId = _computerId;
}

bool Relation::setBuilt(int i)
{
    if (i == 0)
        return false;
    else
        return true;
}
