#include "addrelationdialog.h"
#include "ui_addrelationdialog.h"
#include <QString>
#include <QDebug>

AddRelationDialog::AddRelationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddRelationDialog)
{
    scienceService = ScienceService();
    ui->setupUi(this);
    getAllScientists();
    getAllComputers();
}

AddRelationDialog::~AddRelationDialog()
{
    delete ui;
}

//gets all computers from db and calls displayScientists
void AddRelationDialog::getAllScientists() {
    currentScientists = scienceService.getAllScientists();
    displayScientists();
}

//displays all scientists to lhs table
void AddRelationDialog::displayScientists() {

    ui->table_S->clearContents();

    ui->table_S->setRowCount(currentScientists.size());
    list<Scientist> currentlyDisplayedScientists;
    for(list<Scientist>::iterator i = currentScientists.begin(); i != currentScientists.end(); ++i) {
        Scientist currentScientist = *i;


        QString SciName = QString::fromStdString(currentScientist.getName());
        QString SciYob = QString::fromStdString(currentScientist.getYearOfBirth());
        QString SciYod = QString::fromStdString(currentScientist.getYearOfDeath());
        QString SciGender = QString::fromStdString(currentScientist.getGender());

        int currentRow = currentlyDisplayedScientists.size();

        ui->table_S->setItem(currentRow, 0, new QTableWidgetItem(SciName));
        ui->table_S->setItem(currentRow, 1, new QTableWidgetItem(SciYob));
        ui->table_S->setItem(currentRow, 2, new QTableWidgetItem(SciYod));
        ui->table_S->setItem(currentRow, 3, new QTableWidgetItem(SciGender));

       currentlyDisplayedScientists.push_back(currentScientist);
    }
}

//gets computers from db and calls displayComputers
void AddRelationDialog::getAllComputers() {
    currentComputers = scienceService.getComputersOrderedBy("1", "+");

    displayComputers();
}

//displays all computers to the rhs table
void AddRelationDialog::displayComputers() {
    ui->table_C->clearContents();

    ui->table_C->setRowCount(currentComputers.size());
    list<Computer> currentlyDisplayedComputers;

    for(list<Computer>::iterator i = currentComputers.begin(); i != currentComputers.end(); ++i) {
        Computer currentComputer = *i;

        QString ComName = QString::fromStdString(currentComputer.getName());
        QString ComYear = QString::number(currentComputer.getYear());
        QString ComType = QString::fromStdString(currentComputer.getType());
        QString ComBuilt = QString::number(currentComputer.getBuilt());

        int currentRow = currentlyDisplayedComputers.size();

        ui->table_C->setItem(currentRow, 0, new QTableWidgetItem(ComName));
        ui->table_C->setItem(currentRow, 1, new QTableWidgetItem(ComYear));
        ui->table_C->setItem(currentRow, 2, new QTableWidgetItem(ComType));
        ui->table_C->setItem(currentRow, 3, new QTableWidgetItem(ComBuilt));

       currentlyDisplayedComputers.push_back(currentComputer);
    }
}

//adds the selcted scientist and computer to the relation sql table
void AddRelationDialog::on_add_Relation_clicked()
{

    scienceService.addRelationIndexes(scientistIndex, computerIndex);

    this->close();
}

//finds the instance of scientist that was selceted
void AddRelationDialog::on_table_S_clicked(const QModelIndex &index)
{
    unsigned int si = (ui->table_S->currentIndex().row()); // index of currentScientists which was selected in the table

    std::list<Scientist>::iterator it = currentScientists.begin();
    advance(it, si);                                       // 'it' points to the element at index 'si'
    scientistIndex = it->getID();                               // id of selected scientist in the database
}

//finds the instance of computer that was selcted
void AddRelationDialog::on_table_C_clicked(const QModelIndex &index)
{
    int ci= (ui->table_C->currentIndex().row());           // index of currentComputers which was selected in the table

    std::list<Computer>::iterator it = currentComputers.begin();
    advance(it, ci);                                       // 'it' points to the element at index 'si'
    computerIndex = it->getID();                                // id of selected scientist in the database
}
