#ifndef REMOVESCIENTISTDIALOG_H
#define REMOVESCIENTISTDIALOG_H

#include <QDialog>
#include "scienceservice.h"

namespace Ui {
class RemoveScientistDialog;
}

class RemoveScientistDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RemoveScientistDialog(QWidget *parent = 0);
    ~RemoveScientistDialog();
    ScienceService scienceService;
    list<Scientist> currentScientists;

    void getAllScientists();
    void displayScientists();

    int scientistIndex;
private slots:

    void on_remove_tableWidget_clicked(const QModelIndex &index);

    void on_remove_pushButton_clicked();

private:
    Ui::RemoveScientistDialog *ui;
};

#endif // REMOVESCIENTISTDIALOG_H
