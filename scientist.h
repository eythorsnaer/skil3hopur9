#ifndef SCIENTIST_H
#define SCIENTIST_H

#include <ctime>
#include <string>

using namespace std;

// DTO for scientists
class Scientist {
public:
    Scientist();
    Scientist(string name, string dateOfBirth, string dateOfDeath, string gender, string id, string imagePath);
    string getImagePath();
    string getName();
    string getYearOfBirth();
    string getYearOfDeath();
    string getGender();
    int getID();
    int getPersonID();

    void setImagePath(string _imagePath);
    void setName(string _name);
    void setYearOfBirth(string _dateOfBirth);
    void setYearOfDeath(string _dateOfDeath);
    void setGender(string _gender);
    void setPersonID(int _personId);
    void setID(int _id);
    // This is required for the remove functionality
    bool operator==(const Scientist &rhs);
private:
    string name;
    string dateOfBirth;
    string dateOfDeath;
    string gender;
    string imagePath;

    int id;

    int personId;


};

#endif // SCIENTIST_H
