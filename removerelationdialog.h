#ifndef REMOVERELATIONDIALOG_H
#define REMOVERELATIONDIALOG_H

#include <QDialog>
#include "scienceservice.h"

namespace Ui {
class RemoveRelationDialog;
}

class RemoveRelationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RemoveRelationDialog(QWidget *parent = 0);
    ~RemoveRelationDialog();

    ScienceService scienceService;
    list<Relation> currentRelations;
    void getAllRelations();
    void displayRelations();
    int relationSciIndex;
    int relationComIndex;
    bool removed;
private slots:


    void on_relation_tableWidget_clicked(const QModelIndex &index);

    void on_removerelation_pushButton_clicked();

private:
    Ui::RemoveRelationDialog *ui;

};

#endif // REMOVERELATIONDIALOG_H
