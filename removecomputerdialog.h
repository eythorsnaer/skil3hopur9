#ifndef REMOVECOMPUTERDIALOG_H
#define REMOVECOMPUTERDIALOG_H

#include <QDialog>
#include "scienceservice.h"

namespace Ui {
class RemoveComputerDialog;
}

class RemoveComputerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RemoveComputerDialog(QWidget *parent = 0);
    ~RemoveComputerDialog();
    void getAllComputers();
    void displayComputers();
    ScienceService scienceService;
    list<Computer> currentComputers;
    int computerIndex;
private slots:
    void on_remove_tableWidget_clicked(const QModelIndex &index);

    void on_remove_pushButton_clicked();

private:
    Ui::RemoveComputerDialog *ui;
};

#endif // REMOVECOMPUTERDIALOG_H
