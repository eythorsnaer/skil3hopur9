#ifndef SCIENTISTREPOSITORY_H
#define SCIENTISTREPOSITORY_H

#include <list>
#include "Scientist.h"
#include <stdexcept>
#include <string>
#include <algorithm>
#include <QtSql>

using namespace std;

class ScientistRepository {
public:
    ScientistRepository();
    ~ScientistRepository();
    void initializeDatabase();
    void add(Scientist);
    std::list<Scientist> list();
    std::list<Scientist> list(std::string col,std::string mod);
    std::list<Scientist> search(std::string searchTerm);
    void remove(int id);
private:
    std::list<Scientist> scientistList;
    QSqlDatabase db;
};

#endif // SCIENTISTREPOSITORY_H
