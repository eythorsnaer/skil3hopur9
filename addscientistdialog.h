#ifndef ADDSCIENTISTDIALOG_H
#define ADDSCIENTISTDIALOG_H

#include <QDialog>
#include "mainwindow.h"

namespace Ui {
class addScientistDialog;
}

class addScientistDialog : public QDialog
{
    Q_OBJECT

public:
    explicit addScientistDialog(QWidget *parent = 0);
    ~addScientistDialog();
    ScienceService scienceService;
    bool scientistInputIsValid();
    void clearAddScientistErrors();

private slots:
    void on_addScientist_clicked();

    void on_pushButton_AddImage_clicked(bool checked);

private:
    Ui::addScientistDialog *ui;
};

#endif // ADDSCIENTISTDIALOG_H
