#include "removerelationdialog.h"
#include "ui_removerelationdialog.h"
#include "mainwindow.h"
#include <QMessageBox>

RemoveRelationDialog::RemoveRelationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RemoveRelationDialog)
{
    ui->setupUi(this);
    scienceService = ScienceService();
    getAllRelations();
}

RemoveRelationDialog::~RemoveRelationDialog()
{
    delete ui;
}

//gets all relations from db and calls displayRelation
void RemoveRelationDialog::getAllRelations() {
    currentRelations = scienceService.getAllRelation();
    displayRelations();
}

//displays all relations
void RemoveRelationDialog::displayRelations() {

    ui->relation_tableWidget->clearContents();

    ui->relation_tableWidget->setRowCount(currentRelations.size());
    list<Relation> currentlyDisplayedRelations; //counter for rows in table
    for(list<Relation>::iterator i = currentRelations.begin(); i != currentRelations.end(); ++i) {
        Relation currentRelation = *i;

        QString rsname = QString::fromStdString(currentRelation.getSName()); //computer member var to qstring
        QString rname = QString::fromStdString(currentRelation.getName());
        QString ryear = QString::number(currentRelation.getYear());
        QString rtype = QString::fromStdString(currentRelation.getType());
        QString rbuilt = QString::number(currentRelation.getBuilt());
        QString rpid = QString::number(currentRelation.getPersonID());
        QString rcid = QString::number(currentRelation.getComputerID());

        int currentRow = currentlyDisplayedRelations.size();

        ui->relation_tableWidget->setItem(currentRow, 0, new QTableWidgetItem(rsname)); //adds member var of computer to table
        ui->relation_tableWidget->setItem(currentRow, 1, new QTableWidgetItem(rname));
        ui->relation_tableWidget->setItem(currentRow, 2, new QTableWidgetItem(ryear));
        ui->relation_tableWidget->setItem(currentRow, 3, new QTableWidgetItem(rtype));
        ui->relation_tableWidget->setItem(currentRow, 4, new QTableWidgetItem(rbuilt));
        ui->relation_tableWidget->setItem(currentRow, 5, new QTableWidgetItem(rpid));
        ui->relation_tableWidget->setItem(currentRow, 6, new QTableWidgetItem(rcid));

       currentlyDisplayedRelations.push_back(currentRelation);
    }
}

//finds the id of computer and scientist in the database
void RemoveRelationDialog::on_relation_tableWidget_clicked(const QModelIndex &index)
{
    unsigned int ri = (ui->relation_tableWidget->currentIndex().row()); /* index of the element we want to retrieve */;

    std::list<Relation>::iterator it = currentRelations.begin();
    advance(it, ri); // 'it' points to the element at index 'ri'
    relationSciIndex = it->getPersonID();
    relationComIndex = it->getComputerID();

}

//removes relation from database
void RemoveRelationDialog::on_removerelation_pushButton_clicked()
{
    int reply = QMessageBox::question(this, "Confirm", "Are you sure you want to remove this relation? ");
    if(reply == QMessageBox::Yes)
    {
    removed = scienceService.removeRelation(relationSciIndex, relationComIndex);
    this->close();
    }
    if(reply == QMessageBox::No)
    {
    this->close();
    }
}
