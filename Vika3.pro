#-------------------------------------------------
#
# Project created by QtCreator 2014-12-11T13:28:38
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Vika3
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    computer.cpp \
    computerrepository.cpp \
    relation.cpp \
    relationrepository.cpp \
    scienceservice.cpp \
    scientist.cpp \
    scientistrepository.cpp \
    addscientistdialog.cpp \
    addcomputerdialog.cpp \
    addrelationdialog.cpp \
    computertypes.cpp \
    removescientistdialog.cpp \
    removecomputerdialog.cpp \
    removerelationdialog.cpp

HEADERS  += mainwindow.h \
    computer.h \
    computerrepository.h \
    relation.h \
    relationrepository.h \
    scienceservice.h \
    scientist.h \
    scientistrepository.h \
    addscientistdialog.h \
    addcomputerdialog.h \
    addrelationdialog.h \
    computertypes.h \
    removescientistdialog.h \
    removecomputerdialog.h \
    removerelationdialog.h

FORMS    += mainwindow.ui \
    addrelationdialog.ui \
    removescientistdialog.ui \
    removecomputerdialog.ui \
    removerelationdialog.ui \
    addscientistdialog.ui \
    addcomputerdialog.ui

RESOURCES += \
    logos.qrc \
    scientists.qrc \
    computer.qrc

OTHER_FILES += \
    ../build-Vika3-Desktop_Qt_Qt_Version_clang_64bit-Debug/Citizen-Science-logo.jpg \
    ../build-Vika3-Desktop_Qt_Qt_Version_clang_64bit-Debug/computer-icon.png \
    ../build-Vika3-Desktop_Qt_Qt_Version_clang_64bit-Debug/relationIcon.png
