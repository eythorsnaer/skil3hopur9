#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
     scienceService = ScienceService();
    ui->setupUi(this);
    currentScientistSortColumn = "Name";
    ui->search_All->setPlaceholderText("Search here....");
    getAllScientists();
    getAllComputers();
    getAllRelations();
}

MainWindow::~MainWindow()
{
    delete ui;
}

//gets scientist from database, depending on the search string and calls displayScientists
void MainWindow::getAllScientists() {
    scienceService = ScienceService();

    if (searchString != "")
        currentScientists = scienceService.search(searchString); //gets a list of all scientists containing the searchString
    else
        currentScientists = scienceService.getAllScientists();

    displayScientists();
}

//displays scientist to table under scientist tab
void MainWindow::displayScientists() {

    ui->table_Scientists->clearContents();

    ui->table_Scientists->setRowCount(currentScientists.size());
    list<Scientist> currentlyDisplayedScientists;   //row counter
    for(list<Scientist>::iterator i = currentScientists.begin(); i != currentScientists.end(); ++i) {
        Scientist currentScientist = *i;

        QString SciName = QString::fromStdString(currentScientist.getName());
        QString SciYob = QString::fromStdString(currentScientist.getYearOfBirth());
        QString SciYod = QString::fromStdString(currentScientist.getYearOfDeath());
        QString SciGender = QString::fromStdString(currentScientist.getGender());

        int currentRow = currentlyDisplayedScientists.size();

        ui->table_Scientists->setItem(currentRow, 0, new QTableWidgetItem(SciName, currentRow));  //adds currentScientist to table
        ui->table_Scientists->setItem(currentRow, 1, new QTableWidgetItem(SciYob, currentRow));
        ui->table_Scientists->setItem(currentRow, 2, new QTableWidgetItem(SciYod, currentRow));
        ui->table_Scientists->setItem(currentRow, 3, new QTableWidgetItem(SciGender, currentRow));

       currentlyDisplayedScientists.push_back(currentScientist);
    }
}

//gets computers from database depending on searchstring. calls displayComputers
void MainWindow::getAllComputers() {
    scienceService = ScienceService();

    if(searchString != "")
        currentComputers = scienceService.searchCom(searchString);
    else
        currentComputers = scienceService.getAllComputers();

    displayComputers();
}

// displays computers to a table under computer tab
void MainWindow::displayComputers() {
    ui->table_Computers->clearContents();

    ui->table_Computers->setRowCount(currentComputers.size());
    list<Computer> currentlyDisplayedComputers; //row counter

    for(list<Computer>::iterator i = currentComputers.begin(); i != currentComputers.end(); ++i) {
        Computer currentComputer = *i;

        QString ComName = QString::fromStdString(currentComputer.getName());  //computer member var to qstring
        QString ComYear = QString::number(currentComputer.getYear());
        QString ComType = QString::fromStdString(currentComputer.getType());
        QString ComBuilt = QString::fromStdString(currentComputer.getBuiltString());

        int currentRow = currentlyDisplayedComputers.size();

        ui->table_Computers->setItem(currentRow, 0, new QTableWidgetItem(ComName, currentRow)); //adds currentComputer to table
        ui->table_Computers->setItem(currentRow, 1, new QTableWidgetItem(ComYear, currentRow));
        ui->table_Computers->setItem(currentRow, 2, new QTableWidgetItem(ComType, currentRow));
        ui->table_Computers->setItem(currentRow, 3, new QTableWidgetItem(ComBuilt, currentRow));

       currentlyDisplayedComputers.push_back(currentComputer);
    }
}

//geta all relations from database depending on searchstring; calls displayRelations
void MainWindow::getAllRelations() {

    if(searchString != "")
        currentRelations = scienceService.searchRelation(searchString);
    else
        currentRelations = scienceService.getRelationsOrderedBy("1", "+");

    displayRelations();
}

//displays relations from currenRelations list
void MainWindow::displayRelations() {

    ui->remove_label->hide();
    ui->remove_label_2->hide();     //hides labes which tells user if adding relation worked

    ui->table_Relations->clearContents();

    ui->table_Relations->setRowCount(currentRelations.size());
    list<Relation> currentlyDisplayedRelations; //row counter

    for(list<Relation>::iterator i = currentRelations.begin(); i != currentRelations.end(); ++i) {
        Relation currentRelation = *i;


        QString persName = QString::fromStdString(currentRelation.getSName());
        QString comName = QString::fromStdString(currentRelation.getName());
        QString comYear = QString::number(currentRelation.getYear());
        QString comType = QString::fromStdString(currentRelation.getType());
        QString comBuilt = QString::fromStdString(currentRelation.getBuiltString());
        QString pID = QString::number(currentRelation.getPersonID());
        QString cID = QString::number(currentRelation.getComputerID());

        int currentRow = currentlyDisplayedRelations.size();


            ui->table_Relations->setItem(currentRow, 0, new QTableWidgetItem(persName));    //adds currentRealtion to table
            ui->table_Relations->setItem(currentRow, 1, new QTableWidgetItem(comName));
            ui->table_Relations->setItem(currentRow, 2, new QTableWidgetItem(comYear));
            ui->table_Relations->setItem(currentRow, 3, new QTableWidgetItem(comType));
            ui->table_Relations->setItem(currentRow, 4, new QTableWidgetItem(comBuilt));
            ui->table_Relations->setItem(currentRow, 5, new QTableWidgetItem(pID));
            ui->table_Relations->setItem(currentRow, 6, new QTableWidgetItem(cID));

            currentlyDisplayedRelations.push_back(currentRelation);


    }
}

//displays new window that prompts user for scientist info
void MainWindow::on_addScientistButton_clicked()
{
    addScientistDialog addScientistDia;
    addScientistDia.exec();

    getAllScientists();
}

//displays new window that prompts user for computer info
void MainWindow::on_add_Computer_clicked()
{
    addComputerDialog addComputerDia;
    addComputerDia.exec();

    getAllComputers();
}

//displays new window; prompts user for relation between scientist and computer
void MainWindow::on_add_Relation_clicked()
{
    AddRelationDialog addRelationDia;
    addRelationDia.exec();

    getAllRelations();
}

//changes serachString every time search bar text is changed and displays all tables again
void MainWindow::on_search_All_textChanged(const QString &arg1)
{
    searchString = ui->search_All->text().toStdString();
    getAllScientists();
    getAllComputers();
    getAllRelations();
}

// adds image to scientist table
void MainWindow::setScientist(Scientist scientist)
{

    ui->label_nameScientist->setText(QString("<h1>%1</h1>").arg(QString::fromStdString(scientist.getName())));
    QPixmap pixmap(QString::fromStdString(scientist.getImagePath()));   // get image path from database

    int imageLabelWidth = ui->label_Image_here->width();
    QPixmap scaledPixMap = pixmap.scaledToWidth(imageLabelWidth); //scale image

    ui->label_Image_here->setPixmap(scaledPixMap);
}

//adds image to computer table
void MainWindow::setComputer(Computer computer)
{


    ui->label_name_computer->setText(QString("<h1>%1</h1>").arg(QString::fromStdString(computer.getName())));
    QPixmap pixmap(QString::fromStdString(computer.getImagePath())); //get image path from database


    int imageLabelWidth = ui->label_image_computer->width();
    QString SHIT = QString::number(imageLabelWidth);

    QPixmap scaledPixMap = pixmap.scaledToWidth(imageLabelWidth); //scale image

    ui->label_image_computer->setPixmap(scaledPixMap);
}

// finds computer that is selected and calls setComputer; displays image connected to that computer
void MainWindow::on_table_Computers_itemSelectionChanged()
{
    QList<QTableWidgetItem *> Comp_selected = ui->table_Computers->selectedItems();
        if (Comp_selected.isEmpty()){
            return;
        }
        QTableWidgetItem* selected = Comp_selected.first();
        int row = selected->type();
        std::list<Computer>::iterator it = currentComputers.begin();
        advance(it, row); // 'it' points to the element at index 'row'
        Computer C = *it;

    setComputer(C);
}

// finds scientist that is selected and calls setScientist; displays image connected to that scientist
void MainWindow::on_table_Scientists_itemSelectionChanged()
{
    QList<QTableWidgetItem *> Sci_selected = ui->table_Scientists->selectedItems();
        if (Sci_selected.isEmpty()){
            return;
        }
        QTableWidgetItem* selected = Sci_selected.first();
        int row = selected->type(); //index of selected scientist
        std::list<Scientist>::iterator it = currentScientists.begin();
        advance(it, row); // 'it' points to the element at index 'row'
        Scientist S = *it;

    setScientist(S);
}

//displays new window; prompts for which scientist to remove
void MainWindow::on_removeScientistButton_clicked()
{
    RemoveScientistDialog removeScientistDialog;
    removeScientistDialog.exec();

    getAllScientists();     //display table again
}

//displays new window; prompts for which computer to remove
void MainWindow::on_remove_Computer_clicked()
{
    RemoveComputerDialog removeComputerDialog;
    removeComputerDialog.exec();

    getAllComputers();      //display table again
}

//displays new window; prompts for which relation to remove
void MainWindow::on_remove_Relation_clicked()
{
    RemoveRelationDialog removeRelationDialog;
    removeRelationDialog.exec();

    getAllRelations();

    if(removeRelationDialog.removed) {
        ui->remove_label->show();       //tells user he removed relation successfully
    }
    else {
        ui->remove_label_2->show();     //tells user something went wrong with removeing relation
    }
}
