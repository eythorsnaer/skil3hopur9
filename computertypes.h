#ifndef COMPUTERTYPES_H
#define COMPUTERTYPES_H

#include <ctime>
#include <string>

class ComputerTypes
{
public:
    ComputerTypes();

    int typeId;
    std::string type;

};

#endif // COMPUTERTYPES_H
