#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "scienceservice.h"
#include "addscientistdialog.h"
#include "addcomputerdialog.h"
#include "addrelationdialog.h"
#include "removescientistdialog.h"
#include "removecomputerdialog.h"
#include "removerelationdialog.h"
#include <list>
#include <qDebug>
#include <algorithm>
#include <QFileDialog>
#include <QPixmap>
#include <QTableWidget>
#include <QTableWidgetItem>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void getAllScientists();
    string searchString;
    list<Scientist> currentScientists;
    list<Computer> currentComputers;
    list<Relation> currentRelations;
    void displayScientists();
    void displayComputers();
    void getAllComputers();
    void getAllRelations();
    void displayRelations();
    void setScientist(Scientist scientist);
    void setComputer(Computer computer);

private slots:
    void on_addScientistButton_clicked();

    void on_add_Computer_clicked();

    void on_add_Relation_clicked();
    
    void on_search_All_textChanged(const QString &arg1);

    void on_table_Scientists_itemSelectionChanged();

    void on_table_Computers_itemSelectionChanged();

    void on_removeScientistButton_clicked();

    void on_remove_Computer_clicked();

    void on_remove_Relation_clicked();

private:
    Ui::MainWindow *ui;
    ScienceService scienceService;
    QString currentScientistSortColumn;

};

#endif // MAINWINDOW_H
