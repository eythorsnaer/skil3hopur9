#ifndef RELATIONREPOSITORY_H
#define RELATIONREPOSITORY_H

#include <list>
#include "relation.h"
#include <stdexcept>
#include <string>
#include <algorithm>
#include <QtSql>
#include <iostream>

using namespace std;

class RelationRepository
{
public:
    RelationRepository();
    ~RelationRepository();
    void add(Relation);
    std::list<Relation> search(std::string searchTerm);
    std::list<Relation> searchcom(std::string searchTerm);
    std::list<Relation> list(std::string col, std::string mod);
    void initializeDatabase();
    void add(int sci, int com);
    bool removeRelation(int sid, int cid);
    std::list<Relation> listAllRelations();
private:
    std::list<Relation> RelationList;
    QSqlDatabase db;
};

#endif // RELATIONREPOSITORY_H
