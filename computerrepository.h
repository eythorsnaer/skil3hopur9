#ifndef COMPUTERREPOSITORY_H
#define COMPUTERREPOSITORY_H

#include <list>
#include "Scientist.h"
#include "computer.h"
#include "relation.h"
#include "computertypes.h"
#include <stdexcept>
#include <string>
#include <algorithm>
#include <QtSql>

using namespace std;

// Handles all the things that pertain to persistence
// uses a file that is located in the build root directory
// for persistent storage
class ComputerRepository {
public:
    ComputerRepository();
    ~ComputerRepository();
    void add(Computer);
    std::list<Computer> listCom();
    std::list<Computer> searchCom(std::string searchTerm);
    std::list<Computer> listComp(std::string col, string mod);
    void initializeDatabase();
    std::list<ComputerTypes> computerTypes();
    std::list<Computer> list();
    void remove(int id);
private:


    // This list is maintained in memory and persisted with ScientistRepository::save()
    std::list<Computer> ComputerList;
    std::list<Computer> searchComputer;
    // The filename of the file that is used to persist data
    std::string filename;
    std::string imagePath;
    // The character that delimits each column of a line
    char delimiter;
    // Persist the private list to a file
     QSqlDatabase db;
    void save();
};

#endif // COMPUTERREPOSITORY_H
