#ifndef SCIENCESERVICE_H
#define SCIENCESERVICE_H

#include "scientistrepository.h"
#include "ComputerRepository.h"
#include "relationrepository.h"
#include "computertypes.h"
#include <list>

using namespace std;

// Domain layer, services the presentation layer
class ScienceService {
public:
    ScienceService();
    ~ScienceService();
    void addScientist(Scientist scientist);
    void addComputer(Computer computer);
    void addRelation(Relation relation);
    // Returns the first scientist that matches the searchTerm
    list<Scientist> search(string searchTerm);
    list<Relation> searchRelation(string searchTerm);
    list<Relation> searchRelationCom(string searchTerm);
    list<Scientist> getAllScientists();
    list<ComputerTypes> getComputerTypes();
    list<Scientist> getScientistsOrderedBy(string,string);
    list<Computer> getAllComputers();
    list<Relation> getAllRelation();
    list<Computer> searchCom(string searchTerm);
    list<Computer> getComputersOrderedBy(string col, string mod);
    list<Relation> getRelationsOrderedBy(string col, string mod);
    void addRelationIndexes(int sci, int com);
    bool removeRelation(int id, int cid);
    void removeComputer(int id);
    void removeScientist(int id);
private:
    ScientistRepository scientistRepository;
    ComputerRepository computerRepository;
    RelationRepository relationRepository;

};

#endif // SCIENCESERVICE_H
