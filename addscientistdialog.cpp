#include "addscientistdialog.h"
#include "ui_addscientistdialog.h"
#include <QFileDialog>
#include <QRegularExpression>
#include <QRegularExpressionMatch>

addScientistDialog::addScientistDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addScientistDialog)
{
    ui->setupUi(this);
}

addScientistDialog::~addScientistDialog()
{
    delete ui;
}

//if input is valid; adds scientist to the database
void addScientistDialog::on_addScientist_clicked()
{

    if(scientistInputIsValid())
    {
        scienceService = ScienceService();

        Scientist additionalScientist = Scientist();

        additionalScientist.setName        ( ui->addScientistName->text().toStdString());
        additionalScientist.setYearOfBirth ( ui->addScientistBorn->text().toStdString());
        additionalScientist.setYearOfDeath ( ui->addScientistDeath->text().toStdString());
        additionalScientist.setImagePath   ( ui->Browse_for_image->text().toStdString());
        if (ui->Female->isChecked()){
            additionalScientist.setGender("Female");
        } else if (ui->Male->isChecked()){
            additionalScientist.setGender("Male");
        }

        scienceService.addScientist(additionalScientist);

        this->close();
    }
}

//clears error tags
void addScientistDialog::clearAddScientistErrors()
{
    ui->errorName->setText("");
    ui->bornError->setText("");
}

//checkes if the input is valid
bool addScientistDialog::scientistInputIsValid()
{
    clearAddScientistErrors();

    bool isValid = true;

    if(ui->addScientistName->text().isEmpty())
    {
        ui->errorName->setText("Name must be entered.");
        isValid = false;
    }


    QRegularExpression rx2("([01][0-9][0-9][0-9]|([2][0][0][0-9])|([2][0][1][0-4]))");      //regex year valid 1000-1999
    QRegularExpression rx3("([2][0][0][0-9])|([2][0][1][0-4])");                            //regex year valid 2000-2014
    if(ui->addScientistBorn->text().isEmpty())
    {
        ui->bornError->setText("Year born cannot be empty");
        isValid = false;
    }


     if(((rx2.match(ui->addScientistBorn->text()).hasMatch())||
       (rx3.match(ui->addScientistBorn->text()).hasMatch())))
    {
        isValid = true;
    }

    else{
            ui->bornError->setText("Year born must be a number (1000-2014)");
            isValid = false;
        }
     if(((rx2.match(ui->addScientistDeath->text()).hasMatch())||
       (rx3.match(ui->addScientistDeath->text()).hasMatch())))
    {
        ui->deathError->setText("");
        isValid = true;
    }

<<<<<<< HEAD
//    else{
//            ui->deathError->setText("Year of death must be a number (1000-2014)");
//            isValid = false;
//        }
=======
>>>>>>> d06e646eb190146ecc207b8c4a62ea93337b0d41
    if(!ui->addScientistDeath->text().isEmpty())
    {
        if(ui->addScientistDeath->text()<ui->addScientistBorn->text())
        {
            ui->deathError->setText("Year of death must be after year of born");
            isValid = false;
        }
        else if(((!rx2.match(ui->addScientistDeath->text()).hasMatch())||
                 (!rx3.match(ui->addScientistDeath->text()).hasMatch())))
        {
            ui->deathError->setText("Year of death must be a number (1000-2014)");
            isValid = false;
        }
    }
    if (!ui->Female->isChecked() && !ui->Male->isChecked()){
        ui->genderError->setText("Gender must be selected");
        isValid = false;
    }
    return isValid;
}

//add image
void addScientistDialog::on_pushButton_AddImage_clicked(bool checked)
{
    QString filename = QFileDialog::getOpenFileName(
                    this,
                    "Browse for image",
                    "",
                    "Image files (*.jpg *.png *.jpeg)"
                );
    ui->Browse_for_image->setText(filename);
}
