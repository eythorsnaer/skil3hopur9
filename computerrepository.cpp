#include "computerrepository.h"
#include <iostream>

using namespace std;

ComputerRepository::ComputerRepository() {
    initializeDatabase();
}
ComputerRepository::~ComputerRepository() {
    db.close();
}
void ComputerRepository::initializeDatabase(){
    QString connectionName = "databaseConnect";
    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName("skil2.sqlite");
        db.open();
    }
}
// Add computer to databade
void ComputerRepository::add(Computer computer) {


    initializeDatabase();
    QSqlQuery query(db);

    query.prepare("INSERT INTO Computers (ComputerName, InventionYear, TypeId, Built, ImagePath)"
                  "VALUES (:name, :Year, :Type, :Built, :imagePath)");
    query.bindValue(":name",        QString::fromStdString(computer.getName()));
    query.bindValue(":Year",        QString::number(computer.getYear()));
    query.bindValue(":Type",        QString::fromStdString(computer.getType()));
    query.bindValue(":Built",       QString::number(computer.getBuilt()));
    query.bindValue(":imagePath",   QString::fromStdString(computer.getImagePath()));

    query.exec();
}

// select computers from database in requested order and the resault to memory
std::list<Computer> ComputerRepository::listComp(std::string col, std::string mod) {

    std::list<Computer> outlist;
    initializeDatabase();
    QSqlQuery query(db);

    std::string orderBy = "";

    if(!col.empty()){
        mod = mod.empty() ? "ASC" : (mod.find("desc") != std::string::npos ? "DESC" : "ASC");
        orderBy = "order by " + col + " " + mod;

    }

    query.exec("SELECT c.ComputerID,c.ComputerName,c.InventionYear,bv.Builtnotbuilt, CT.type FROM Computers c "
               "left join Computertypes CT ON CT.typeId = c.typeId "
               "left join BoolValues BV ON BV.ZeroOne = c.built " + QString::fromStdString(orderBy));

    while (query.next()) {
         Computer c;

         c.setID(query.value("ComputerID").toString().toInt());//)      = query.value("ComputerID").toString().toInt();
         c.setName(query.value("ComputerName").toString().toStdString());     //fáum qstring til baka úr toString þurfum þess vegna að gera toStdString
         c.setType(query.value("type").toString().toStdString());
         c.setYear(query.value("InventionYear").toString().toInt());
         c.setBuiltString(query.value("BuiltNotBuilt").toString().toStdString());
         c.setImagePath(query.value("imagePath").toString().toStdString());

         outlist.push_back(c);
    }
    return outlist;

}
std::list<Computer> ComputerRepository::list(){

    std::list<Computer> outlist;
    initializeDatabase();
    QSqlQuery query(db);
    Computer c1;

    query.exec("SELECT c.ComputerID,c.ComputerName,c.InventionYear,bv.Builtnotbuilt, CT.type, c.imagePath FROM Computers c "
               "left join Computertypes CT ON CT.typeId = c.typeId "
               "left join BoolValues BV ON BV.ZeroOne = c.built ");
    while(query.next()){

         c1.setID(query.value("ComputerID").toString().toInt());
         c1.setName(query.value("ComputerName").toString().toStdString());     //fáum qstring til baka úr toString þurfum þess vegna að gera toStdString
         c1.setType(query.value("type").toString().toStdString());
         c1.setYear(query.value("InventionYear").toString().toInt());
         c1.setBuiltString(query.value("BuiltNotBuilt").toString().toStdString());
         c1.setImagePath(query.value("imagePath").toString().toStdString());

//         c1.id        = query.value("ComputerID").toString().toInt();
//         c1.name      = query.value("ComputerName").toString().toStdString();     //fáum qstring til baka úr toString þurfum þess vegna að gera toStdString
//         c1.type      = query.value("type").toString().toStdString();
//         c1.year      = query.value("InventionYear").toString().toInt();
//         c1.builtString     = query.value("Builtnotbuilt").toString().toStdString();
//         c1.imagePath = query.value("imagePath").toString().toStdString();

         outlist.push_back(c1);
    }
    return outlist;
}
// searh for computers in database and adds the resault to memory
std::list<Computer> ComputerRepository::searchCom(std::string searchTerm) {

    Computer com;

    std::list<Computer> searchComputer;
    initializeDatabase();
    QSqlQuery query(db);
    query.prepare("SELECT c.ComputerID,c.ComputerName,c.InventionYear,bv.Builtnotbuilt, CT.type, c.imagePath FROM Computers c "
                  "left join Computertypes CT ON CT.typeId = c.typeId "
                  "left join BoolValues BV ON BV.ZeroOne = c.built "
                  "Where c.ComputerName LIKE '%'|| ? || '%'");
    query.addBindValue(QString::fromStdString(searchTerm));
    query.exec();

    while(query.next()) {
        com.setID(query.value("ComputerID").toString().toInt());
        com.setName(query.value("ComputerName").toString().toStdString());     //fáum qstring til baka úr toString þurfum þess vegna að gera toStdString
        com.setType(query.value("type").toString().toStdString());
        com.setYear(query.value("InventionYear").toString().toInt());
        com.setBuiltString(query.value("BuiltNotBuilt").toString().toStdString());
        com.setImagePath(query.value("ImagePath").toString().toStdString());
        searchComputer.push_back(com);
     }
    return searchComputer;
}

// List of computer types to memory
std::list<ComputerTypes>  ComputerRepository::computerTypes() {

    ComputerTypes type;
    std::list<ComputerTypes> typeComputer = std::list<ComputerTypes>();
    initializeDatabase();
    QSqlQuery query(db);
    query.prepare("SELECT * FROM ComputerTypes");

    query.exec();

    while(query.next()) {
        type.typeId     = query.value("TypeId").toString().toInt();
        type.type       = query.value("Type").toString().toStdString();

        typeComputer.push_back(type);
     }
    return typeComputer;
}

void ComputerRepository::remove(int id) {
    QSqlQuery query(db);

    query.prepare("DELETE FROM Computers WHERE ComputerID = :id");
    query.bindValue(":id", id);

    query.exec();

    query.prepare("DELETE FROM PersCompRelation WHERE ComputerId = :id");

    query.bindValue(":id", id);

    query.exec();
}



