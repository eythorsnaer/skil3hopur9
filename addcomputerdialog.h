#ifndef ADDCOMPUTERDIALOG_H
#define ADDCOMPUTERDIALOG_H

#include <QDialog>
#include "mainwindow.h"
#include <string>

namespace Ui {
class addComputerDialog;
}

class addComputerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit addComputerDialog(QWidget *parent = 0);
    ~addComputerDialog();
    ScienceService scienceService;
    list<ComputerTypes> currentTypes;
    string ct;
    bool computerInputIsValid();
    void clearAddComputerErrors();

private slots:
    void on_pushButton_clicked();

    void on_Browse_for_image_clicked(bool checked);

    void on_type_comboBox_activated(int index);

private:
    Ui::addComputerDialog *ui;
    int computerTypeIndex;

};

#endif // ADDCOMPUTERDIALOG_H
