#include "removecomputerdialog.h"
#include "ui_removecomputerdialog.h"
#include <QMessageBox>

RemoveComputerDialog::RemoveComputerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RemoveComputerDialog)
{
    ui->setupUi(this);
    scienceService = ScienceService();
    getAllComputers();
}

RemoveComputerDialog::~RemoveComputerDialog()
{
    delete ui;
}


//gets computers from db and calls displayComputers
void RemoveComputerDialog::getAllComputers() {
    currentComputers = scienceService.getAllComputers();

    displayComputers();
}

//displays all computers to the rhs table
void RemoveComputerDialog::displayComputers() {
    ui->remove_tableWidget->clearContents();

    ui->remove_tableWidget->setRowCount(currentComputers.size());
    list<Computer> currentlyDisplayedComputers; //counter for table (currentRow)

    for(list<Computer>::iterator i = currentComputers.begin(); i != currentComputers.end(); ++i) {
        Computer currentComputer = *i;

        QString ComName = QString::fromStdString(currentComputer.getName());
        QString ComYear = QString::number(currentComputer.getYear());
        QString ComType = QString::fromStdString(currentComputer.getType());
        QString ComBuilt = QString::number(currentComputer.getBuilt());

        int currentRow = currentlyDisplayedComputers.size();

        ui->remove_tableWidget->setItem(currentRow, 0, new QTableWidgetItem(ComName));
        ui->remove_tableWidget->setItem(currentRow, 1, new QTableWidgetItem(ComYear));
        ui->remove_tableWidget->setItem(currentRow, 2, new QTableWidgetItem(ComType));
        ui->remove_tableWidget->setItem(currentRow, 3, new QTableWidgetItem(ComBuilt));

       currentlyDisplayedComputers.push_back(currentComputer);  //counter++
    }
}

//finds id of computer that is selected in the table
void RemoveComputerDialog::on_remove_tableWidget_clicked(const QModelIndex &index)
{
    int ci= (ui->remove_tableWidget->currentIndex().row()); //index of computer selected in table

    std::list<Computer>::iterator it = currentComputers.begin();
    advance(it, ci); // 'it' points to the element at index 'ci'
    computerIndex = it->getID(); //id of computer in the database
}

//calls remove computer in repository; removes the computer from the database
void RemoveComputerDialog::on_remove_pushButton_clicked()
{
    int reply = QMessageBox::question(this, "Confirm", "Are you sure you want to remove this computer? ");
    if(reply == QMessageBox::Yes)
    {
    scienceService.removeComputer(computerIndex);
    this->close();
    }
    if(reply == QMessageBox::No)
    {
    this->close();
    }
}
