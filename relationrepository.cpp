#include "relationrepository.h"

RelationRepository::RelationRepository()
{
    initializeDatabase();
}

RelationRepository::~RelationRepository() {
    db.close();
}

void RelationRepository::initializeDatabase(){
    QString connectionName = "databaseConnect";

    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName("skil2.sqlite");

        db.open();
    }
}
// Add relations to database
void RelationRepository::add(Relation relation) {

    initializeDatabase();
    QSqlQuery query(db);
    query.prepare("INSERT INTO PersCompRelation (PersonId, ComputerId)"
                  "VALUES (:PersonId, :ComputerId)");

    query.bindValue(":PersonId",   QString::number(relation.getPersonID()));
    query.bindValue(":ComputerId", QString::number(relation.getComputerID()));

    query.exec();
}

void RelationRepository::add(int sci, int com) {

    initializeDatabase();
    QSqlQuery query(db);
    query.prepare("INSERT INTO PersCompRelation (PersonId, ComputerId)"
                  "VALUES (:PersonId, :ComputerId)");

    query.bindValue(":PersonId", QString::number(sci));
    query.bindValue(":ComputerId", QString::number(com));

    query.exec();
}

// List of Scientist and Computer relation selected from DB and added to memory

std::list<Relation> RelationRepository::list(std::string col, std::string mod) {

    std::list<Relation> outlist;
    QSqlQuery query(db);

    string orderBy = "";

    if(!col.empty()){
        mod = mod.empty() ? "ASC" : (mod.find("desc") != std::string::npos ? "DESC" : "ASC");
        orderBy = "order by " + col + " " + mod;

    }
    query.exec("select * from persons p "
               "left join PersCompRelation cmp on p.personId = cmp.PersonId "
               "left join Computers C on cmp.ComputerId = C.ComputerId "
               "left join computerTypes CT ON c.typeid = CT.typeId "
               "left join BoolValues BV ON BV.ZeroOne = C.built "+ QString::fromStdString(orderBy));


    while (query.next()) {
         Relation rel;
         rel.setPersonID(    query.value("PersonID").toString().toInt());
         rel.setSName(       query.value("Name").toString().toStdString());
         rel.setName(     query.value("ComputerName").toString().toStdString());   // fáum qstring til baka úr toString þurfum þess vegna að gera toStdString
         rel.setType(        query.value("Type").toString().toStdString());
         rel.setYear(        query.value("InventionYear").toString().toInt());
         rel.setBuiltString( query.value("BuiltNotBuilt").toString().toStdString());
         rel.setComputerID(  query.value("computerId").toString().toInt());

         outlist.push_back(rel);
    }
    return outlist;
}

std::list<Relation> RelationRepository::listAllRelations() {

    std::list<Relation> outlist;
    initializeDatabase();
    QSqlQuery query(db);
    Relation rel;
    query.exec("select * from persons p "
               "left join PersCompRelation cmp on p.personId = cmp.PersonId "
               "left join Computers C on cmp.ComputerId = C.ComputerId "
               "left join computerTypes CT ON c.typeid = CT.typeId "
               "left join BoolValues BV ON BV.ZeroOne = C.built "
               "order by Name ");


    while (query.next()){
         rel.setPersonID     (query.value("PersonID").toString().toInt());
         rel.setSName        (query.value("Name").toString().toStdString());
         rel.setName         (query.value("ComputerName").toString().toStdString());     //fáum qstring til baka úr toString þurfum þess vegna að gera toStdString
         rel.setType         (query.value("Type").toString().toStdString());
         rel.setYear         (query.value("InventionYear").toString().toInt());
         rel.setBuiltString  (query.value("BuiltNotBuilt").toString().toStdString());
         rel.setComputerID   (query.value("computerId").toString().toInt());

         outlist.push_back(rel);
    }
    return outlist;
}

// search of scientists theyre relation to computers selected from datadase and added to memory
std::list<Relation>  RelationRepository::search(std::string searchTerm) {

    Relation r;

    std::list<Relation> searchList = std::list<Relation>();
    initializeDatabase();
    QSqlQuery query(db);
    query.prepare("select BV.BuiltNotBuilt,p.Name,C.ComputerName,CT.Type,C.InventionYear,C.Built,p.PersonID, C.computerID from persons p "
                  "inner join PersCompRelation cmp on p.personId = cmp.PersonId "
                  "inner join Computers C on cmp.ComputerId = C.ComputerId "
                  "left join computerTypes CT ON c.typeid = CT.typeId "
                  "left join BoolValues BV ON BV.ZeroOne = C.built "
                  "WHERE p.Name LIKE '%'|| :search || '%' "
                  "OR C.ComputerName LIKE '%'|| :search || '%' ");
    query.bindValue(":search", QString::fromStdString(searchTerm));
    query.exec();

    while(query.next()) {
        r.setSName        ( query.value("Name").toString().toStdString());
        r.setName         ( query.value("ComputerName").toString().toStdString());
        r.setType         ( query.value("Type").toString().toStdString());
        r.setYear         ( query.value("InventionYear").toString().toInt());
        r.setBuiltString  ( query.value("BuiltNotBuilt").toString().toStdString());
        r.setPersonID     ( query.value("PersonID").toString().toInt());
        r.setComputerID   ( query.value("computerId").toString().toInt());
        searchList.push_back(r);
     }
    db.close();
    return searchList;
}

// search of computers theyre relation to scientists selected from datadase and added to memory
std::list<Relation>  RelationRepository::searchcom(std::string searchTerm) {

    Relation r;

    std::list<Relation> searchList = std::list<Relation>();
    initializeDatabase();
    QSqlQuery query(db);
    query.prepare("select BV.BuiltNotBuilt,p.Name,C.ComputerName,CT.Type,C.InventionYear,C.Built from persons p "
                  "inner join PersCompRelation cmp on p.personId = cmp.PersonId "
                  "inner join Computers C on cmp.ComputerId = C.ComputerId "
                  "left join computerTypes CT ON c.typeid = CT.typeId "
                  "left join BoolValues BV ON BV.ZeroOne = C.built "
                  "WHERE c.ComputerName LIKE '%'|| ? || '%' ");
    query.addBindValue(QString::fromStdString(searchTerm));
    query.exec();

    while(query.next()) {
        r.setName( query.value("ComputerName").toString().toStdString());
        r.setSName( query.value("Name").toString().toStdString());
        r.setType( query.value("Type").toString().toStdString());
        r.setYear( query.value("InventionYear").toString().toInt());
        r.setBuiltString( query.value("BuiltNotBuilt").toString().toStdString());
        searchList.push_back(r);
     }
    db.close();
    return searchList;
}

bool RelationRepository::removeRelation(int sid, int cid)
{
    QSqlQuery query(db);

    query.prepare("DELETE FROM PersCompRelation "
                  "WHERE PersonId = :sid "
                  "AND ComputerId = :cid");
    query.bindValue(":sid", sid);
    query.bindValue(":cid", cid);

    bool result = query.exec();
    qDebug() << query.lastError();
    qDebug() << query.lastQuery();
    return result;
}
