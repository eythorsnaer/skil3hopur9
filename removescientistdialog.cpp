    #include "removescientistdialog.h"
#include "ui_removescientistdialog.h"
#include <QMessageBox>

RemoveScientistDialog::RemoveScientistDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RemoveScientistDialog)
{
    ui->setupUi(this);
    scienceService = ScienceService();
    getAllScientists();
}

RemoveScientistDialog::~RemoveScientistDialog()
{
    delete ui;
}

//gets all computers from db and calls displayScientists
void RemoveScientistDialog::getAllScientists() {
    currentScientists = scienceService.getAllScientists();
    displayScientists();
}

//displays all scientists to lhs table
void RemoveScientistDialog::displayScientists() {

    ui->remove_tableWidget->clearContents();

    ui->remove_tableWidget->setRowCount(currentScientists.size());
    list<Scientist> currentlyDisplayedScientists;
    for(list<Scientist>::iterator i = currentScientists.begin(); i != currentScientists.end(); ++i) {
        Scientist currentScientist = *i;

       QString SciName = QString::fromStdString(currentScientist.getName());        //scientist member var. to qstring
       QString SciYob = QString::fromStdString(currentScientist.getYearOfBirth());
       QString SciYod = QString::fromStdString(currentScientist.getYearOfDeath());
       QString SciGender = QString::fromStdString(currentScientist.getGender());

        int currentRow = currentlyDisplayedScientists.size();

        ui->remove_tableWidget->setItem(currentRow, 0, new QTableWidgetItem(SciName));      //display scientist member var. to table
        ui->remove_tableWidget->setItem(currentRow, 1, new QTableWidgetItem(SciYob));
        ui->remove_tableWidget->setItem(currentRow, 2, new QTableWidgetItem(SciYod));
        ui->remove_tableWidget->setItem(currentRow, 3, new QTableWidgetItem(SciGender));

       currentlyDisplayedScientists.push_back(currentScientist);
    }
}

//finds id in database of selected scientist
void RemoveScientistDialog::on_remove_tableWidget_clicked(const QModelIndex &index)
{
    unsigned int si = (ui->remove_tableWidget->currentIndex().row()); //index of scientist in table

    std::list<Scientist>::iterator it = currentScientists.begin();
    advance(it, si); // 'it' points to the element at index 'si'
    scientistIndex = it->getID();
}

//removes scientist from database
void RemoveScientistDialog::on_remove_pushButton_clicked()
{
    int reply = QMessageBox::question(this, "Confirm", "Are you sure you want to remove this scientist? ");
    if(reply == QMessageBox::Yes)
    {
    scienceService.removeScientist(scientistIndex);
    this->close();
    }
    if(reply == QMessageBox::No)
    {
    this->close();
    }
}
