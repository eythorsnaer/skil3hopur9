#ifndef COMPUTER_H
#define COMPUTER_H

#include <ctime>
#include <string>

class Computer
{
public:
    Computer();
    bool operator==(const Computer &rhs);
    std::string getImagePath();
    std::string getName();
    int getYear();
    std::string getType();
    bool getBuilt();
    std::string getBuiltString();
    int getID();
    int getCtypeID();
    void setImagePath(std::string _imagePath);
    void setName(std::string _name);
    void setYear(int _year);
    void setType(std::string _type);
    void setID(int _id);
    void setBuilt(int i);
    void setBuiltString(std::string _BuiltString);



private:
    std::string name;
    std::string type;
    std::string imagePath;
    bool built;
    std::string BuiltString;
    int ctypeid;
    int id;
    int computerId;
    int year;
};

#endif // COMPUTER_H



