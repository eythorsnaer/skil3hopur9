#include "scientistrepository.h"
#include <iostream>

using namespace std;

ScientistRepository::ScientistRepository() {

    initializeDatabase();
}
ScientistRepository::~ScientistRepository() {

    db.close();
}

void ScientistRepository::initializeDatabase(){
    QString connectionName = "databaseConnect";

    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName("skil2.sqlite");

        db.open();
    }
}

// Add scientist to database
void ScientistRepository::add(Scientist scientist) {

    initializeDatabase();
    QSqlQuery query(db);
    query.prepare("INSERT INTO Persons (Name, Born, Death, Gender, ImagePath)"
                  "VALUES (:name, :dateOfBirth, :dateOfDeath, :gender, :imagePath)");

    query.bindValue(":name", QString::fromStdString(scientist.getName()));
    query.bindValue(":dateOfBirth", QString::fromStdString(scientist.getYearOfBirth()));
    query.bindValue(":dateOfDeath", QString::fromStdString(scientist.getYearOfDeath()));
    query.bindValue(":gender", QString::fromStdString(scientist.getGender()));
    query.bindValue(":imagePath",QString::fromStdString(scientist.getImagePath()));

    query.exec();
}

// List of Scientist and Computer relation selected from DB and added to memory
std::list<Scientist> ScientistRepository::list() {

    initializeDatabase();
    QSqlQuery query(db);
    query.exec("SELECT * FROM Persons");

    while (query.next()) {
         Scientist sl;
         sl.setID (          query.value("PersonID").toString().toInt());         //fáum qstring til baka úr toString þurfum þess vegna að gera toStdString
         sl.setName (        query.value("Name").toString().toStdString());
         sl.setYearOfBirth ( query.value("Born").toString().toStdString());
         sl.setYearOfDeath ( query.value("Death").toString().toStdString());
         sl.setGender (      query.value("Gender").toString().toStdString());
         sl.setImagePath (   query.value("ImagePath").toString().toStdString());

         scientistList.push_back(sl);
    }
    return scientistList;
}

// List of scientists selected from datadase in requested order and added to memory to display
std::list<Scientist> ScientistRepository::list(std::string col, std::string mod) {

    std::list<Scientist> outlist; // = std::list<Scientist>();

    initializeDatabase();
    QSqlQuery query(db);

    string orderBy = "";

    if(!col.empty()){
        mod = mod.empty() ? "ASC" : (mod.find("desc") != std::string::npos ? "DESC" : "ASC");
        orderBy = "order by " + col + " " + mod;

    }
    query.exec("SELECT * FROM Persons p ORDER by p.:col :mod" + QString::fromStdString(orderBy));

    while (query.next()) {
         Scientist sl;
         sl.setID (          query.value("PersonID").toString().toInt());
          sl.setName (        query.value("Name").toString().toStdString());     //fáum qstring til baka úr toString þurfum þess vegna að gera toStdString
          sl.setYearOfBirth ( query.value("Born").toString().toStdString());
          sl.setYearOfDeath ( query.value("Death").toString().toStdString());
          sl.setGender (      query.value("Gender").toString().toStdString());
          sl.setImagePath (   query.value("FilePath").toString().toStdString());
         outlist.push_back(sl);
    }
    return outlist;
}
// search of scientists selected from datadase and added to memory
std::list<Scientist>  ScientistRepository::search(std::string searchTerm) {

    Scientist s;

    std::list<Scientist> searchList = std::list<Scientist>();
    initializeDatabase();
    QSqlQuery query(db);
    query.prepare("SELECT * FROM Persons p WHERE p.Name LIKE '%'|| ? || '%'");
    query.addBindValue(QString::fromStdString(searchTerm));
    query.exec();

    while(query.next()) {
        s.setID          ( query.value("PersonID").toString().toInt());
        s.setName        ( query.value("Name").toString().toStdString());
        s.setYearOfBirth ( query.value("Born").toString().toStdString());
        s.setYearOfDeath ( query.value("Death").toString().toStdString());
        s.setGender      ( query.value("Gender").toString().toStdString());
        s.setImagePath   ( query.value("ImagePath").toString().toStdString());
        searchList.push_back(s);
     }
    return searchList;
}

void ScientistRepository::remove(int id) {
    QSqlQuery query(db);

    query.prepare("DELETE FROM Persons WHERE PersonID = :id");
    query.bindValue(":id", id);

    query.exec();

    query.prepare("DELETE FROM PersCompRelation WHERE PersonId = :id");
    query.bindValue(":id", id);

    query.exec();
}
