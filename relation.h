#ifndef RELATION_H
#define RELATION_H

#include <ctime>
#include <string>

class Relation
{
public:
    Relation();
    bool operator==(const Relation &rhs);

    std::string getName();
    std::string getSName();
    std::string getType();
    std::string getBuiltString();
    int getYear();
    int getPersonID();
    int getComputerID();
    bool getBuilt();

    void setName(std::string _name);
    void setSName(std::string _sname);
    void setType(std::string _type);
    void setBuiltString(std::string _builtString);
    void setYear(int _year);
    void setPersonID(int _personId);
    void setComputerID(int _computerId);
    bool setBuilt(int i);

private:
    std::string sname;
    std::string name;
    int year;
    std::string type;
    bool built;
    std::string builtString;
    int personId;
    int computerId;

};

#endif // RELATION_H
