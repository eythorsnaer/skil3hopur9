#include "addcomputerdialog.h"
#include "ui_addcomputerdialog.h"
#include <QFileDialog>
#include <QRegularExpression>

addComputerDialog::addComputerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addComputerDialog)
{

    scienceService = ScienceService();
    ui->setupUi(this);
}

addComputerDialog::~addComputerDialog()
{
    delete ui;
}

//if the input is valid; adds computer to the database
void addComputerDialog::on_pushButton_clicked()
{

    if(computerInputIsValid())
    {
        Computer additionalComputer = Computer();
        additionalComputer.setName(ui->addComputerName->text().toStdString());
        additionalComputer.setYear(ui->addInventionYear->text().toInt());
        additionalComputer.setType(ct);
        additionalComputer.setImagePath(ui->image_location->text().toStdString());
        if (ui->BuiltButtonYes->isChecked()){
            additionalComputer.setBuilt(1);
        } else if (ui->BuiltButtonNo->isChecked()){
            additionalComputer.setBuilt(0);
        }

        scienceService.addComputer(additionalComputer);

        this->close();
    }
}

//clears error tags
void addComputerDialog::clearAddComputerErrors()
{
    ui->errorComputer->setText("");
    ui->errorYear->setText("");
}

//checkes if the input is valid
bool addComputerDialog::computerInputIsValid()
{
    clearAddComputerErrors();

    bool isValid = true;

    if(ui->addComputerName->text().isEmpty())
    {
        ui->errorName->setText("Name cannot be empty");
        isValid = false;
    }
    QRegularExpression rx1("[0-9]");
    QRegularExpression rx11("[0-9][0-9]");
    QRegularExpression rx111("[0-9][0-9][0-9]");
    QRegularExpression rx2("([01][0-9][0-9][0-9]|([2][0][0][0-9])|([2][0][1][0-4]))");      //regex year valid 1000-1999
    QRegularExpression rx3("([2][0][0][0-9])|([2][0][1][0-4])");                            //regex year valid 2000-2014

    if(((rx2.match(ui->addInventionYear->text()).hasMatch())||
        (rx3.match(ui->addInventionYear->text()).hasMatch())))
    {
        isValid = true;
    }
    else{
        ui->errorYear->setText("Year born must be a number (1000-2014)");
        isValid = false;
      }
    if(ui->addInventionYear->text().isEmpty())
    {
        ui->errorYear->setText("Invention year cannot be empty");
        isValid = false;
    }
    QRegularExpression rx("^[0-9]{1,4}$");                      //regex year validation
    if(!(rx.match(ui->addInventionYear->text()).hasMatch()))
    {
        ui->errorYear->setText("Invention year must be\n a number (0-9999)");
        isValid = false;
    }
    if (!ui->BuiltButtonYes->isChecked() && !ui->BuiltButtonNo->isChecked()){
        ui->errorBuilt->setText("Add built or not built");
        isValid = false;
    }

    return isValid;
}

//add image
void addComputerDialog::on_Browse_for_image_clicked(bool checked)
{
    QString filename = QFileDialog::getOpenFileName(
                    this,
                    "Browse for image",
                    "",
                    "Image files (*.jpg *.png *.jpeg)"
                );
    ui->image_location->setText(filename);
}

//index in combox selected to string
void addComputerDialog::on_type_comboBox_activated(int index)
{
    QString temp = QString::number(index);
    ct = temp.toStdString();

}
